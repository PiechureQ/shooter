var GameObject = require('../../lib/GameObject');
const nosync = require("colyseus").nosync;

module.exports = class Player extends GameObject.ControlableObject{
    constructor(props) {
        super(props);
        
        nosync(this, "body");
        this.body = props.body;

        this.name = props.name;

        this.weapon = props.weapon;
        this.weapon.setParent(this);
        // this.weapons = []

        //to know witch entity owns witch body
        this.body.matterBody.owner = this.clientId;

        //omg fucking temp
        this.body.matterBody.ownerRef = this;
	}

	spawn(pos){
        this.revive();
        if(pos){
            this.setPosition(pos.x, pos.y);
        }
    }

    shoot(time){
        if(this.weapon.canShoot(time))
            return this.weapon.shoot({x: this.x, y: this.y}, this.turn);
        else
            return false;
    }
};