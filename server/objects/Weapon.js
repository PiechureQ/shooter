const nosync = require("colyseus").nosync;
var GameObject = require('../../lib/GameObject');

class Weapon extends GameObject.CarryableObject{
    constructor(data) {
        super(data);
        this.magazineSize = 30;
        this.ammo = 30;

        this.interval = 100; // in ms
        this.lastTime = 0;

        // not sure if this is good
        this.detector = undefined;
        nosync(this, 'detector');
        this.detector = data.detector;
    }

    canShoot(time){
        return time >= this.lastTime + this.interval;
    }

    setParent(parent){
        this.parentId = parent.clientId;
    }
}

class AutomaticWeapon extends Weapon{
    constructor(data){
        super(data);
    }

    shoot(startPosition, direction) {
        this.lastTime = Date.now();
        return this.detector.rayIntersection(startPosition, direction, this.parentId);
    }
}

module.exports = {
    AutomaticWeapon: AutomaticWeapon
};