var Observer = require("../../lib/Observer").Observer;
var Physics = require("../../lib/Physics").Physics;
var Detector = require("../../lib/Detector").Detector;
var Map = require("../../lib/Map").Map;
const nosync = require("colyseus").nosync;

var physics = new Physics();
var detector = new Detector(physics.bodies, physics.composites);

const PLAYER_RESP = {x: 500, y: -200};

module.exports = class StateHandler extends Observer{
    constructor() {
        super();
        this.players = {};
        this.time = 0;

        this.physics = undefined;
        nosync(this, "physics");
        this.physics = physics;
        this.detector = undefined;
        nosync(this, "detector");
        this.detector = detector;
        detector.addObserver(this);
    }

    update(dataQueue, delta) {
        this.physics.simulate(delta);
        dataQueue.forEach((input, index) => {
            var player = this.getPlayer(input.clientId);
            if(player) {
                if(player.isDead){
                    if(input.revive){
                        player.spawn(PLAYER_RESP);
                    }
                } else {
                    player.move(input);
                    player.turn = input.turn;
                    player.setPosition(player.body.getPosition().x, player.body.getPosition().y);

                    //shooting
                    if (input.shoot) {
                        //todo strzelanie pojedyncze a seria jak zrobic
                        player.shoot(Date.now());
                    }
                }
                dataQueue.splice(index, 1);

            }
        });
        this.time = Date.now();
    }

    onNotify(clientId, event){
        //todo event types
        if(this.players[clientId]){
            let entity = this.players[clientId];
            entity.health -= 20;
            //im scared
            if(entity.health < 0 && !entity.isDead) {
                this.getPlayer(event.author).addScore();
                entity.isDead = true;
            }
        }
    }

    addPlayer(clientId, player) {
        this.physics.addBody(player.body);
        this.players[clientId] = player;
    }

    getPlayer(clientId) {
        return this.players[clientId];
    }

    removePlayer(clientId) {
        this.physics.removeBody(this.players[clientId].body);
        delete this.players[clientId];
    }

    createMap(JSON){
        let loaded = {val: false};
        Map.fromJSON(JSON).then((mapInfo)=>{
            this.physics.addBody(Map.createMap(mapInfo));
            loaded.val = true;
            console.log("map created")
        }).catch((err)=>{
            return false;
        });
        return loaded;
    }
};