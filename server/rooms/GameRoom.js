const colyseus = require("colyseus");
const StateHandler = require("./StateHandler");
const Player = require("../objects/Player");
const Weapon = require('../objects/Weapon');
const PhysicalBody= require("../../lib/Physics").PhysicalBody;
const Categories = require('../../lib/Physics').Categories;

// console.log(new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {label: "player", inertia: Infinity, friction: 0, restitution: 0, collisionFilter: {category: Categories.PLAYERS, mask: Categories.DEFAULT}}));
var stateHandler = new StateHandler();
const PLAYER_RESP = {x: 500, y: -200};
const PLAYER_SIZE = {width: 45, height: 70};
module.exports = class GameRoom extends colyseus.Room {
    constructor() {
        super(...arguments);
        this.maxClients = 8;
        this.dataQueue = [];
    }

    onInit(options) {
        this.patchRate = 33;
        this.setState(stateHandler);
        this.setSimulationInterval(() => this.onUpdate(), 33);

        this.isMapLoaded = this.state.createMap("client/assets/maps/forest.json");
    }

    requestJoin(options) {
        return true;
    }

    onJoin(client, options) {
        //TODO valid options.name check if etc

        // var options = {label: "player", inertia: Infinity, friction: 0, restitution: 0, collisionFilter: {category: Categories.PLAYERS, mask: Categories.DEFAULT}};
        var body = new PhysicalBody({position: PLAYER_RESP, width: PLAYER_SIZE.width, height: PLAYER_SIZE.height}, {label: "player", inertia: Infinity, friction: 0, restitution: 0, collisionFilter: {category: Categories.PLAYERS, mask: Categories.DEFAULT}});
        var weapon = new Weapon.AutomaticWeapon({x: PLAYER_RESP.x, y: PLAYER_RESP.y, rotation: 0, detector: this.state.detector});

        let player = new Player({
            id: client.id,
            name: options.name,
            x: PLAYER_RESP.x,
            y: PLAYER_RESP.y,
            rotation: 0,
            body: body,
            weapon: weapon
        });
        
        this.state.addPlayer(client.id, player);
    }

    onMessage(client, data) {
        this.dataQueue.push(data);
    }

    onUpdate() {
        if (this.isMapLoaded.val) {
            this.state.update(this.dataQueue, this.clock.deltaTime);
            this.broadcast(this.state);
        }
    }

    onLeave(client) {
        this.dataQueue.forEach((data, index) => {
            if (data.clientId === client.id)
                this.dataQueue.splice(index, 1);
        });
        // if player exist remove him / not if player was destroyed when he died
        if (this.state.players[client.id])
            this.state.removePlayer(client.id);
    }
};