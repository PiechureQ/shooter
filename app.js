const path = require("path");
const express = require("express");
// const favicon = require('serve-favicon');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require("cors");

const app = express();

var LOGGER = (process.env.NODE_ENV === "development") ? "dev" : "short";

app.use(logger(LOGGER));
app.use(cors());
app.use(bodyParser.json());
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, "public")));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
});

module.exports = app;