var path = require('path');
var entryPrefix = __dirname + '/shared/';

module.exports = function(env, argv) {
    if(!env) env = {};

    var MODE = (env.prod) ? "production" : "development";

    return {
        mode: MODE,

        entry: {
            GameObject: entryPrefix + 'classes/GameObject.js',
            Physics: entryPrefix + 'Physics.js',
            Detector: entryPrefix + 'Detector.js',
            Observer: entryPrefix + 'classes/Observer.js',
            Map: entryPrefix + 'Map.js',
            Buffer: entryPrefix + 'classes/Buffer.js'
        },
        devtool: 'cheap-source-map',
        output: {
            path: path.resolve(__dirname, 'lib'),
            filename: '[name].js',
            library: '[name]',
            libraryTarget: 'umd',
            globalObject: 'this',
            umdNamedDefine: true
        },
        target: 'node',


        module: {
            rules: [
                {
                    test: /\.js?$/,
                    exclude: [/node_modules/]
                }
            ]
        },

    }
};