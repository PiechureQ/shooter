const webpack = require('webpack');
const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function(env, argv) {
    if(!env) env = {};

    var MODE = (env.prod) ? "production" : "development";

    return {
        mode: MODE,

        entry: [
            path.resolve('client/index.js'),
            path.resolve('client/css/style.css')
        ],

        output: {
            path: path.resolve(__dirname, 'public'),
            filename: 'bundle.js'
        },
        //todo none if prod
        devtool: 'cheap-source-map',
        target: 'web',

        module: {
            rules: [
                {   //babel
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.(js|jsx)?$/,
                    exclude: [/node_modules/, /vendors/]
                },
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract({ fallback: "style-loader", use: "css-loader" })
                }
            ]
        },

        plugins: [
            new ExtractTextPlugin("styles.css"),
            new HtmlWebpackPlugin({
                template: path.resolve("client", "index.html")
            }),
            new CopyWebpackPlugin([
                {
                    from: path.resolve(__dirname, 'client/assets'),
                    to: path.resolve(__dirname, 'public/assets')
                }
            ]),
            new webpack.DefinePlugin({
                'CANVAS_RENDERER': JSON.stringify(true),
                'WEBGL_RENDERER': JSON.stringify(true)
            }),
            //todo
            // new webpack.SplitChunksPlugin({
            //     name: 'production-dependencies',
            //     filename: 'production-dependencies.bundle.js'
            // }),
        ],

        // Disable MAX 250kb entrypoint warnings on console
        performance: { hints: false },

        devServer: {
            contentBase: path.resolve(__dirname, 'public'),
        },

        resolve: {
            extensions: ['.js', '.json', '.jsx']
        }

    }
};