import fs from 'fs';
import {Categories} from '../lib/Physics';
import Matter from "matter-js";

var Bodies = Matter.Bodies,
    Composite = Matter.Composite;

export class Map{

    static createMap(mapInfo){
        //works on one map file for now
        var row = 0,
            col = 0,
            width = mapInfo.width,
            height = mapInfo.height,
            tileHeight = mapInfo.tileHeight,
            tileWidth = mapInfo.tileWidth,
            blockWidth = 0,
            positionsSum = 0,
            layers = mapInfo.layers,
            origin = {x: 0, y: -1 * height * tileHeight};

        //create parent object to hold every map child
        var mapComposite = Composite.create({isStatic: true});

        layers.forEach((layer, i) => {
            if(layer.name.toLowerCase().trim() !== "foreground" && layer.name.toLowerCase().trim() !== "background"){

                layer.data.forEach((d, i) => {
                    if (d !== 0){
                        blockWidth += 1;
                        positionsSum += (col * tileWidth) + origin.x + tileWidth/2;
                        //sum posisions of tiles and count them
                        if (layer.data[i + 1] < 1 || col >= width - 1){
                            var options = {isStatic: true, friction: 0, collisionFilter: { mask: Categories.DEFAULT | Categories.PLAYERS, category: Categories.DEFAULT}};
                            //add rectangle to map                                 x = avg of positions
                            var body = Bodies.rectangle(positionsSum / blockWidth, (row * tileHeight) + origin.y + tileHeight/2, tileWidth * blockWidth, tileHeight, options);
                            body.tag = layer.name.toLowerCase().trim();
                            Composite.add(mapComposite, body);
                            blockWidth = 0;
                            positionsSum = 0;
                        }
                    }
                    col += 1;
                    if (col > width - 1){
                        col = 0;
                        row += 1;
                    }
                });
                row = 0;
                col = 0;
            }
        });

        return mapComposite;
    }

    static fromJSON(jsonPath){
        //yey i can async
        return new Promise(function (resolve, reject){
            fs.readFile(jsonPath, 'utf8', function (err, data) {
                if (err) reject(err);
                var obj = JSON.parse(data);
                resolve({
                    height: obj.height,
                    width: obj.width,
                    tileHeight: obj.tileheight,
                    tileWidth: obj.tilewidth,
                    layers: obj.layers
                });
            });
        });
    }
};