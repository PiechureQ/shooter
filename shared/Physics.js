import Matter from "matter-js";

var Engine = Matter.Engine,
    World = Matter.World,
    Body = Matter.Body,
    Bodies = Matter.Bodies,
    Vector = Matter.Vector,
    Events = Matter.Events;

export class Physics{
    constructor(){
        this.engine = Engine.create();
        this.bodies = this.engine.world.bodies;
        this.composites = this.engine.world.composites;

        this.addEvents();
        //add ground to world so players wont fall forever
        // World.add(this.engine.world, Bodies.rectangle(0, 0, 10000, 100, { isStatic: true }));
    }

    simulate(delta){
        Engine.update(this.engine, delta)
    }

    addBody(body){
        let matterBody = body.matterBody ? body.matterBody : body;
        World.add(this.engine.world, matterBody);
    }

    removeBody(body){
        let matterBody = body.matterBody ? body.matterBody : body;
        World.remove(this.engine.world, matterBody);
    }

    addEvents(){
        //chckig if players is on the ground
        Events.on(this.engine, 'collisionStart', function(event) {
            var pairs = event.pairs;

            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i];
                if (pair.bodyA.tag === "ground" && pair.bodyB.tag === "player")
                    pair.bodyB.onGround = true;
                if(pair.bodyB.tag === "ground" && pair.bodyA.tag === "player")
                    pair.bodyA.onGround = true;
            }
        });
        Events.on(this.engine, 'collisionEnd', function(event) {
            var pairs = event.pairs;

            // change object colours to show those ending a collision
            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i];
                if (pair.bodyA.tag === "ground" && pair.bodyB.tag === "player")
                    pair.bodyB.onGround = false;
                if(pair.bodyB.tag === "ground" && pair.bodyA.tag === "player")
                    pair.bodyA.onGround = false;
            }
        });

    }
}

export class PhysicalBody{
    constructor(props, options){
        var body = Bodies.rectangle(props.position.x, props.position.y, props.width, props.height, options);
        body.tag = "player";
        this.matterBody = body; // created to enable adding body to matter world
    }

    setPosition(x, y){
        Body.setPosition(this.matterBody, Vector.create(x, y));
    }

    getPosition(){
        return this.matterBody.position;
    }

    setVelocity(velocity) {
        Body.setVelocity(this.matterBody, Vector.create(velocity.x, velocity.y))
    }

    getVelocity(){
        return this.matterBody.velocity;
    }

    isOnGround(){
        return this.matterBody.onGround;
    }
}

export var Categories = {
    DEFAULT: 0x0001,
    PLAYERS: 0x0002
};