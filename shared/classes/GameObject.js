export class GameObject{
    constructor(props){
        this.x = props.x;
        this.y = props.y;
        this.rotation = props.rotation;
    }

    setPosition(x, y){
		this.x = x;
		this.y = y;
    }

	rotate(angle){
		this.rotation = angle;
	}
};

export class CarryableObject extends GameObject{
	constructor(props){
		// super({position: props.parent.position, rotation: props.parent.rotation});
		super(props);
		// this.parent = props.parent;
	}
}
// static - can't move
export class StaticObject extends GameObject{
	constructor(props){
		super(props);
	}
}
export class PickableObject extends StaticObject{
	constructor(props){
		super(props);
	}
};

// moving - has ability to move
export class MovingObject extends GameObject{
	constructor(props){
		super(props);
		//need to initialize this property for nosync function to work
		this.body = props.body;
		this.revive();
	}
	revive(){
		this.health = 100;
		this.isDead = false;
	}

	setPosition(x, y){
		this.x = x;
		this.y = y;
		this.body.setPosition(x, y);
	}
	move(data){
		if (data.move.right) {
			this.body.setVelocity({x: Math.min(this.body.getVelocity().x + 1, 10), y: this.body.getVelocity().y});
		}else if (data.move.left) {
			this.body.setVelocity({x: Math.max(this.body.getVelocity().x - 1,-10), y: this.body.getVelocity().y});
		}else {
			//slow down effect
			let x = this.body.getVelocity().x;
			if (x > 0)
				x = x > 0.3 ? x - 0.3 : 0;
			else
				x = x < -0.3 ? x + 0.3 : 0;
			this.body.setVelocity({x: x, y: this.body.getVelocity().y});
		}
		if (data.move.jump && this.body.isOnGround()){
			this.body.setVelocity({x: this.body.getVelocity().x, y: -20});
		}
	}
};

export class ControlableObject extends MovingObject{
	constructor(props){
		super(props);
		this.clientId = props.id;
		this.score = 0;
	}

	addScore(val){
		if (val){
			this.score += val;
		} else {
			this.score += 1;
		}
	}
};