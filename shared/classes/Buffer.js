export class Buffer{
    constructor(){
        this.buffer = [];
    }

    getOne(index){
        if(index) return this.buffer[index]
    }

    push(item){
        if (this.buffer.length > 99)
            this.buffer = this.buffer.slice(1);
        this.buffer.push(item);
    }

    clear(){
        this.buffer = [];
    }
}