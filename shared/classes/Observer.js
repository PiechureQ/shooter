export class Observer{
	constructor(){

	}

  	onNotify(entity, event){

  	}
}

export class Subject{
	constructor(){
		this.observers = [];
		this.numObservers = 0;	
	}

  	addObserver(observer){
    	this.observers.push(observer);
    	this.numObservers = this.observers.length;
  	}

  	removeObserver(observer){
    	this.observers.splice(this.observers.indexOf(observer), 1);
    	this.numObservers = this.observers.length;
	}

    notify(entity, event){
    	for (var i = 0; i < this.numObservers; i++){
    		this.observers[i].onNotify(entity, event);
    	}
  	}
}