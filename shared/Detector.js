import Matter from "matter-js";
import {Subject} from "../lib/Observer";

var Query = Matter.Query,
    Vector = Matter.Vector;

export class Detector extends Subject{
    constructor(bodies, composites){
        super();
        this.bodies = bodies;
        this.composites = composites;
    }

    rayIntersection(startPosition, direction, callerId){
        let maxDistance = 1000;
        let bodies = [];
        this.bodies.forEach((body) => {
            if(body.ownerRef && !body.ownerRef.isDead)
                bodies.push(body);
        });
        // index = 0 for map composite
        this.composites[0].bodies.forEach((body) => {
            bodies.push(body);
        });

        //moze byc problem z tym co wysylam na serwer jako direction 
        direction -= Math.PI;
        let x = startPosition.x + 58 * Math.cos(direction);
        let y = startPosition.y + 58 * Math.sin(direction);

        var endPosition = {
            x: startPosition.x + maxDistance * Math.cos(direction),
            y: startPosition.y + maxDistance * Math.sin(direction)
        };

        var intersects = Query.ray(bodies, Vector.create(x, y), Vector.create(endPosition.x, endPosition.y));

        // get closest hit algorithm
        let hit;
        if(intersects.length > 0 && intersects.length < 2){
            return intersects[0].body
        }
        else {
            let least = 0;
            intersects.forEach((body) => {
                //find closest body
                let dis = Math.sqrt(Math.pow(body.body.position.x - startPosition.x, 2) + Math.pow(body.body.position.y - startPosition.y, 2));
                if(least === 0)
                    least = dis;

                if(dis <= least){
                    least = dis;
                    hit = body.body;
                }
            });

            //observer patter
            //if hited object has a owner == is player
            if (hit && hit.owner){
            //todo event object
                this.notify(hit.owner, {event: "hit", author: callerId});
            }
            return hit;
        }
    }
}