const assert = require('chai').assert;
const Matter = require('matter-js');

var GameObject = require("../lib/GameObject");
var PhysicalBody = require('../lib/Physics').PhysicalBody;

describe("MovingObject", () => {

    // translate
    describe("setPosition", () => {
        it("should set position of body with itself", () => {
            const Categories = require('../lib/Physics').Categories;
            let body = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {label: "player", inertia: Infinity, friction: 0, restitution: 0, collisionFilter: {category: Categories.PLAYERS, mask: Categories.DEFAULT}});
            let movObj = new GameObject.MovingObject({
                    x: 500,
                    y: -400,
                    rotation: 0,
                    body: body
            });

            movObj.setPosition(200, 200);

            assert.equal(movObj.x, body.getPosition().x);
            assert.equal(movObj.y, body.getPosition().y);

        })
    })

});