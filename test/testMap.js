const assert = require('chai').assert;
const Matter = require("matter-js");
const Map = require('../lib/Map').Map;

const TEST_MAP = "client/assets/maps/forest_small.json";

describe("Map", () => {
    //constructor
    describe("fromJSON", () => {
        it("should return map props with promise", (done) => {

            Map.fromJSON(TEST_MAP).then(function(mapInfo) {
                try {
                    assert.equal(mapInfo.height, 50);
                    assert.equal(mapInfo.width, 100);
                    assert.equal(mapInfo.tileHeight, 16);
                    assert.equal(mapInfo.tileWidth, 16);
                    assert.isArray(mapInfo.layers);
                    assert.lengthOf(mapInfo.layers, 3);
                    assert.containsAllKeys(mapInfo.layers[0], ["name", "data", "id"]);
                    assert.containsAllKeys(mapInfo.layers[1], ["name", "data", "id"]);
                    assert.containsAllKeys(mapInfo.layers[2], ["name", "data", "id"]);
                    done();
                } catch(err) {
                    done(err);
                }
            }, done);

        });
    });
    describe("createMap", () => {
        it("should return map body composite", (done) => {
            Map.fromJSON(TEST_MAP).then((mapInfo) => {
                var map = Map.createMap(mapInfo);
                assert.typeOf(map, "object");
                assert.equal(map.type, "composite");
                assert.typeOf(map.bodies, "array");
                assert.equal(map.bodies.length, 6);
                done();
            }).catch((reason) => {
                done(reason);
            })
        });
        it("should create body with tag GROUND", (done) => {
            Map.fromJSON(TEST_MAP).then((mapInfo) => {
                var map = Map.createMap(mapInfo);
                var ground;
                map.bodies.forEach((body, i) => {
                    if(body.tag === "ground")
                        ground = body
                });
                assert.equal(ground.tag, 'ground');
                assert.equal(ground.position.x, 480);
                assert.equal(ground.position.y, -88);
                done();
            }).catch((reason) => {
                done(reason);
            })
        });
    })
});