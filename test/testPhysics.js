const assert = require('chai').assert;
const Matter = require('matter-js');

var Physics = require('../lib/Physics').Physics;
var PhysicalBody = require('../lib/Physics').PhysicalBody;

describe("Physics", () => {
    //constructor
    describe("constructor", () => {
        it("should have engine prop", () => {
            let physics = new Physics();
            assert.exists(physics.engine);
        });
        it("should have bodies prop", () => {
            let physics = new Physics();
            assert.exists(physics.bodies);
            assert.equal(physics.bodies.length, 0)
        });
        it("should have composites prop", () => {
            let physics = new Physics();
            assert.exists(physics.composites);
            assert.equal(physics.composites.length, 0)
        });
    });

    // addBody
    describe('addBody', () => {
        let body = Matter.Body.create();
        let wrappedBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
        it('should add matter body', () => {
            let physics = new Physics();
            physics.addBody(body);
            assert.equal(physics.bodies.length, 1);
            assert.strictEqual(physics.bodies[0], body);
        });
        it('should add wrapper body', () => {
            let physics = new Physics();
            physics.addBody(wrappedBody);
            assert.equal(physics.bodies.length, 1);
            assert.strictEqual(physics.bodies[0], wrappedBody.matterBody);
        });
    });

    // removeBody
    describe('removeBody', () => {
        let body = Matter.Body.create();
        let wrappedBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
        it('should add matter body', () => {
            let physics = new Physics();
            physics.addBody(body);
            assert.equal(physics.bodies.length, 1);
            physics.removeBody(body);
            assert.equal(physics.bodies.length, 0);
        });
        it('should add wrapper body', () => {
            let physics = new Physics();
            physics.addBody(wrappedBody);
            assert.equal(physics.bodies.length, 1);
            physics.removeBody(wrappedBody);
            assert.equal(physics.bodies.length, 0);
        })
    })
});

describe("PhysicalBody", () => {
    describe('constructor', () => {
        it('should have referece to original matterjs body', () => {
            let physicalBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
            assert.property(physicalBody, 'matterBody');
        });
    });

    describe('setPosition', () => {
        it('should change position', () => {
            let physicalBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
            physicalBody.setPosition(20, -100);
            assert.equal(physicalBody.getPosition().x, 20);
            assert.equal(physicalBody.getPosition().y, -100);
        })
    });

    describe('getPosition', () => {
        it('should return position', () => {
            let physicalBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
            assert.equal(physicalBody.getPosition().x, 500);
            assert.equal(physicalBody.getPosition().y, -400);
        })
    });

    describe('setVelocity', () => {
        it('should change body velocity', () => {
            let physicalBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
            physicalBody.setVelocity({x: 10, y: 5});
            assert.equal(physicalBody.getVelocity().x, 10);
            assert.equal(physicalBody.getVelocity().y, 5);
        })
    });

    describe('getVelocity', () => {
        it('should return velocity', () => {
            let physicalBody = new PhysicalBody({position: {x: 500, y: -400}, width: 64, height: 96}, {});
            assert.equal(physicalBody.getVelocity().x, 0);
            assert.equal(physicalBody.getVelocity().y, 0);
        })
    });

});