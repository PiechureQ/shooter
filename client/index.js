import Phaser from "phaser"
import PlayScene from "./src/scenes/play"
import MenuScene from "./src/scenes/menu"
import React, {Component} from "react";
import ReactDOM from "react-dom";
import LandingPage from "./components/LandingPage.jsx"
import Revive from "./components/Revive.jsx"
import Leaderboard from "./components/Leaderboard";

class Game extends Component{
    constructor(){
        super();
    }

    shouldComponentUpdate(nextProps, nextState){
        return false;
    }

    render(){
        return (<div id="parentGame" tabIndex={0}/>)
    }
}

class GameApp extends Component{
    constructor(){
        super();
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handleUserRevive = this.handleUserRevive.bind(this);
        this.handleUserDied = this.handleUserDied.bind(this);
        this.handleScore = this.handleScore.bind(this);
        this.handleJoin = this.handleJoin.bind(this);
        this.handleLeave = this.handleLeave.bind(this);

        this.state = {
            username: "",
            logged: false,
            game: {},
            scenes: {},
            focus: false,
            isUserDead: false,
            actualScene: "menu",
            players: []
        }
    }

    componentDidMount(){
        var config = {
            type: Phaser.AUTO,
            width: window.innerWidth,
            height: window.innerHeight,
            parent: "parentGame",
            scene: [MenuScene, PlayScene],
            backgroundColor: '#f3cca3',
        };

        var game = new Phaser.Game(config);

        //listen if player died
        game.events.addListener('playerDied', this.handleUserDied);
        //listen if score of any player has changed
        game.events.addListener('scoreChanged', this.handleScore);
        //listen if player joined/leaved
        game.events.addListener('joined', this.handleJoin);
        game.events.addListener('left', this.handleLeave);

        this.setState({game: game, scenes: game.scene.keys});
    }

    handleUsernameChange(name){
        this.setState({
            username: name,
            logged: true
        });
        // go to play scene if user has name and currently on menuScene
        if(this.state.actualScene === "menu"){
            this.state.scenes.MenuScene.goToScene("PlayScene", {name: name});
            this.setState({
                actualScene: "play"
            });
        }
    }

    handleUserDied(){
        this.setState({isUserDead: true});
    }

    handleUserRevive(){
        this.setState({isUserDead: false});
        this.state.scenes.PlayScene.events.emit("revivePlayer");
    }

    handleScore(entity, newScore){
        this.setState(state => {
            const players = state.players;
            players.forEach((item) => {
                if(item.id === entity)
                    item.score = newScore;
            });

            return {
                players
            }
        })
    }

    handleJoin(entity, player){
        this.setState(state => {
            const players = state.players.concat({name: player.name, id: player.clientId, score: player.score});

            return {
                players,
            };
        });
    }

    handleLeave(entity, player){
        this.setState(state => {
            const players = state.players.filter((item) => item.id !== player.clientId);

            return {
                players,
            };
        });
    }

    render(){
        return (
            <React.Fragment>
                <Game/>
                <LandingPage passUsername={this.handleUsernameChange} className={this.state.logged ? "hide" : "show"}/>
                <Revive revive={this.handleUserRevive} className={this.state.isUserDead ? "show" : "hide"}/>
                <Leaderboard players={this.state.players} className={this.state.logged ? "show" : "hide"}/>
            </React.Fragment>)
    }
}

ReactDOM.render(<GameApp/>, document.getElementById("container"));