import {Component} from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import React from "react";

export default class LandingPage extends Component{
    constructor(){
        super();
        this.state = {username: "", show: true};
    }

    handleName(event){
        this.setState({
            username: event.target.value,
        });
    };

    handlePlay(event){
        if (this.state.username) {
            this.setState({
                show: !this.state.show
            });

            this.props.passUsername(this.state.username)
        }
    }

    render(){
        return (
            <Grid
                id="login"
                className={this.props.className}
                container
                direction="column"
                justify="center"
                alignItems="center">

                <div id="logo"><h1>This is logo</h1></div>

                <TextField
                    id="user_name"
                    className="inputName"
                    label="Name"
                    autoFocus={true}
                    value={this.state.username || ''}
                    onChange={(e) => {this.handleName(e)}}
                    margin="normal"
                    variant="outlined"
                    disabled={!this.state.show}
                />

                <div //div, becouse button is stealling my keyboard
                    id="play_button"
                    color="primary"
                    className="btn"
                    onMouseUp={(e) => {this.handlePlay(e)}}
                >Play!</div>

            </Grid>
        )
    }
}