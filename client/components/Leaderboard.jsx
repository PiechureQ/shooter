import {Component} from "react";
import React from "react";

export default class Leaderboard extends Component{
    constructor(){
        super();
    }

    render(){
        return (
            <div id="leaderboard" className={this.props.className + " scoreContainer"}>

                {this.props.players.map((player) =>
                    <div className="playerSlot" key={player.id}>
                        <span className="playerName">{player.name}</span>
                        <span className="playerScore">{player.score}</span>
                    </div>
                )}

            </div>);
    }
}