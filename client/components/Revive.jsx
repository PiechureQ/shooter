import {Component} from "react";
import React from "react";
import Grid from "@material-ui/core/Grid";

export default class Revive extends Component{
    constructor(){
        super();
        this.state = {isDead: true};
    }

    handleRevive(){
        this.props.revive();
    }

    render(){
        return (
            <Grid
                className={this.props.className}
                id="revive"
                container
                direction="column"
                justify="center"
                alignItems="center">
                <div
                    id="revive_button"
                    color="secondary"
                    className="btn"
                    onClick={(e) => {this.handleRevive(e)}}
                >Revive!</div>
            </Grid>
        );
    }
}