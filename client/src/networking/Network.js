import {Client} from "colyseus.js";
import Player from "../objects/Player";
import {AutomaticWeapon} from "../objects/Weapon";

var HOST = (process.env.NODE_ENV === "development") ? "ws://localhost:3000" : location.origin.replace(/^http/, 'ws');
export const client = new Client(HOST);

export default class Network{
    constructor(playScene){
        this.canSand = false;
        this.room = {};
        this.client = client;

        this.playScene = playScene;
    }

    sendData(data){
        if(this.canSend)
            this.room.send(data);
    }

    joinRoom(name, init, data){
        this.room = this.client.join(name, {name: data.name});
        if (init){
            //on join callback
            let joinCB = () => {
                console.log("you joined", this.client.id);
                this.canSend = true;
            };
            this.addOnJoinListener(joinCB.bind(this));

            //on message callback
            let messageCB = (state) => {
                this.playScene.sync(state);
            };
            this.addOnMessageListener(messageCB.bind(this));

            //listen to room changes
            let roomCB = (change) => {
                if (this.playScene.isLoaded) {
                    if (change.operation === "add") {
                        var weapon = new AutomaticWeapon({scene: this.playScene, sprite: "gun", value: change.value.weapon});
                        var player = new Player({scene: this.playScene, sprite: "player", weapon: weapon, value: change.value});
                        player.show();
                        this.playScene.state.addPlayer(change.path.id, player);
                        if (change.path.id === this.client.id) {
                            this.playScene.state.setPlayer(player);
                        }
                    }

                    if (change.operation === "remove") {
                        this.playScene.state.players[change.path.id].die();
                        this.playScene.state.removePlayer(change.path.id);
                    }
                }
            };
            this.roomListen("players/:id", roomCB.bind(this));
        }
    }

    leaveRoom(name){

    }

    addOnJoinListener(callback){
        this.room.onJoin.add(callback);
    }

    removeOnJoinListener(){

    }

    addOnMessageListener(callback){
        this.room.onMessage.add(callback);
    }

    removeOnMessageListener(){

    }

    roomListen(on, callback){
        this.room.listen(on, callback);
    }
}