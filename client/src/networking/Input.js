import {client} from "./Network"

export default class Input {
	constructor(){
        this.turn = 0;
        this.shoot = false;
        this.reload = false;
        this.revive = false;
        this.sprint = false;
        this.move = {
            jump: false,
            left: false,
            right: false
        };
    }

	get input(){
		return {
		    turn: this.turn,
            shoot: this.shoot,
            reload: this.reload,
            revive: this.revive,
            move: this.move,
            sprint: this.sprint,
            time: Date.now(),
            clientId: client.id
		}
	}
}