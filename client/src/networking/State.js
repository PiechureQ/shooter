import {Subject} from "../../../lib/Observer"

export default class State extends Subject{
    constructor(){
        super();
        this.player;
        this.players = {};
    }

    fromServer(state){
        // if local state != server tell playScene
        if(this.player) {
            this.playerDied(state.players[this.player.clientId]);
        }
        Object.keys(state.players).forEach((clientId)=>{
            var player = this.players[clientId];
            var serverPlayer = state.players[clientId];
            if (player){
                this.playerScoreChange(player, serverPlayer);
                this.updateLocalPlayer(player, serverPlayer);
            }
        }, this)
    }

    updateLocalPlayer(player, serverPlayer){
        player.setPosition(serverPlayer.x, serverPlayer.y);
        player.weapon.rotate(serverPlayer.turn);
        //todo efficient updating
        //todo kurwa ale kupa to jest musze zrobic to jakos fajnie zeby mozna to bylo wygodnie rozszerzac i zmieniac
        player.health = serverPlayer.health;
        player.isDead = serverPlayer.isDead;
        //xd
        player.sprite.visible = !player.isDead;
        player.score = serverPlayer.score;
        player.name = serverPlayer.name;
    }

    //not really sure if this class should do things with local player, need to think
    playerDied(serverPlayer) {
        //check if this.player died
        if (serverPlayer.isDead && serverPlayer.isDead !== this.player.isDead){
            this.notify(this.player.clientId, {event: "died", val: true});
        }
    }

    playerScoreChange(player, serverPlayer) {
        //check every player if score changed
        if(serverPlayer.score !== player.score)
            this.notify(player.clientId, {event: "score", val: serverPlayer.score});
    }

    setPlayer(player){
        player.focusCamera();
        this.player = player;
    }

    addPlayer(clientId, player) {
        this.notify(clientId, {event: "join", player: player});
        this.players[clientId] = player;
    }

    getPlayer(clientId) {
        return this.players[clientId];
    }

    removePlayer(clientId) {
        this.notify(clientId, {event: "leave", player: this.players[clientId]});
        delete this.players[clientId];
    }
}