import {CarryableObject} from '../../../lib/GameObject';
import Sprite from "../classes/Sprite";

class Weapon extends CarryableObject{
	constructor(props){
		super(props.value);
		this.sprite = new Sprite(props);
	}

	shoot(){
		
	}

	rotate(angle){
		this.sprite.rotate(angle);
	}

	setPosition(x, y){
		this.sprite.setPosition(x, y);
	}
}


export class AutomaticWeapon extends Weapon{
	constructor(props){
		super(props);

	}
}