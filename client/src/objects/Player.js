import {ControlableObject} from '../../../lib/GameObject';
import Sprite from "../classes/Sprite";

export default class Player extends ControlableObject{
    constructor(props){
        super(props.value);
        this.sprite = new Sprite(props);

        this.clientId = props.value.clientId;
        this.health = props.value.health;
        this.isDead = props.value.isDead;
        this.name = props.value.name;

        this.weapon = props.weapon;
    }

    focusCamera(){
        this.sprite.focusCamera(this.sprite);
    }

    show(){
        this.sprite.show();
        this.weapon.sprite.show();
    }

    die(){
        this.weapon.sprite.die();
        this.sprite.die();
    }

    setPosition(x, y){
        this.weapon.setPosition(x, y);
        this.sprite.setPosition(x, y);
    }
};

//
// export function ServerPlayer(scene, player){
//     MovingObject.call(this, scene);
//     // this.mesh = player.mesh.clone();
//     this.mesh.name = 'player clone';
//     this.mesh.material.diffuseColor = new BABYLON.Color3(0.8, 0, 1);
// }