import Network from "../networking/Network"
import Controls from "../classes/Controls";
import Input from "../networking/Input";
import State from "../networking/State";
import {Observer} from "../../../lib/Observer";

export default class PlayScene extends Phaser.Scene {
    constructor() {
        super("PlayScene");
        this.controls = new Controls(new Input());
        this.state = new State();
        this.observer = new Observer();
        this.network = new Network(this);
        this.focus = true;


        this.state.addObserver(this.observer);
        this.observer.onNotify = (entity, event) => {
            if(event.event === "died")
                this.game.events.emit("playerDied");
            if(event.event === "score")
                this.game.events.emit("scoreChanged", entity, event.val);
            if(event.event === "join")
                this.game.events.emit("joined", entity, event.player);
            if(event.event === "leave")
                this.game.events.emit("left", entity, event.player);
        }
    }

    preload() {
        //not to get confused, this is path in public folder
        this.load.baseURL = "/assets/";

        this.load.tilemapTiledJSON("map", "maps/forest.json");
        this.load.image("tiles", "tilesets/cliffs.png");

        this.load.image("gun", "AK-47.png");

        // this.load.spritesheet('player',
        //     'player.png',
        //     { frameWidth: 64, frameHeight: 96});

        this.load.spritesheet('player',
            'player.png',
            { frameWidth: 45, frameHeight: 70});
    }

    init(data){
        this.playerName = data.name;
    }

    create() {
        this.isLoaded = true;
        // this.game.world.setBounds(0, 0, 2400, 1200);

        var map = this.make.tilemap({ key: "map"});
        var tileset = map.addTilesetImage("clifs", "tiles");
        map.createStaticLayer(0, tileset, 0, -3200);
        map.createStaticLayer(1, tileset, 0, -3200);
        map.createStaticLayer(2, tileset, 0, -3200);

        this.input.keyboard.on('keydown', this.controls.onKeyDown, this.controls);
        this.input.keyboard.on('keyup', this.controls.onKeyUp, this.controls);
        this.input.on('pointerdown', this.controls.onMouseDown, this.controls);
        this.input.on('pointerup', this.controls.onMouseUp, this.controls);

        //listen for react events
        this.events.addListener("revivePlayer", () => {this.controls.setRevive(true);});

        this.network.joinRoom("game", true, {name: this.playerName});
    }

    update(time, delta) {
        if (this.state.player){
            // calc turn from player to pointer
            this.controls.getMousePos(this.input.mousePointer, this.cameras.main);
            let angle = this.controls.getTurnFromMouse(this.state.player.sprite.x, this.state.player.sprite.y);

            // turn off for now
            if(!this.state.player.isDead)
                this.state.player.weapon.rotate(angle);

            this.controls.sendInput(this.network.sendData.bind(this.network));
        }
    }

    sync(state){
        this.state.fromServer(state);
    }
}