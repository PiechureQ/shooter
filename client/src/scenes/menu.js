export default class MenuScene extends Phaser.Scene{
    constructor(){
        super("MenuScene")
    }

    create(){

    }

    goToScene(name, data){
        this.scene.manager.sleep("MenuScene");
        this.scene.manager.start(name, data);
    }
}