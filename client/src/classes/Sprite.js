export default class Sprite extends Phaser.GameObjects.Sprite{
    constructor(props) {
        super(props.scene, props.value.x, props.value.y, props.sprite);
    }

    show(){
        this.scene.add.existing(this);
    };

    focusCamera(){
        this.scene.cameras.main.startFollow(this);
    }

    die(){
        this.destroy();
    };

    setPosition(x, y){
        this.x = x;
        this.y = y;
    }

    rotate(angle){
        this.rotation = angle;
    }
}