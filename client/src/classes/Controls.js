export default class Controls{
    constructor(input) {
        this.input = input;
        this.idle = true;
    }

    sendInput(send) {
        send(this.input.input);
        if (this.input.revive)
            this.setRevive(false);
    }

    setRevive(v){
        this.input.revive = v;
    }

    getMousePos(mousePointer, camera){
        //get mouse pos
        this.mousePos = mousePointer.positionToCamera(camera);
        return this.mousePos;
    }

    getTurnFromMouse(cx, cy) {
        let theta = Math.atan2(this.mousePos.y - cy, this.mousePos.x - cx) + Math.PI;
        this.input.turn = theta;
        return theta;
    }

    onMouseDown() {
        this.input.shoot = true;
    };

    onMouseUp() {
        this.input.shoot = false;
    };

    onKeyDown( event ) {
        switch ( event.keyCode ) {
            case 38: /*up*/
            case 87: /*W*/ this.input.move.jump = true; break;

            case 37: /*left*/
            case 65: /*A*/ this.input.move.left = true; break;

            case 39: /*right*/
            case 68: /*D*/ this.input.move.right = true; break;

            case 82: /*R*/ this.input.reload = true; break;

            case 27: /*ESC*/ break;
            case 16: /*shift*/ this.input.sprint = true; break;
            case 32: /*space*/ this.input.move.jump = true; break;

        }
    };

    onKeyUp( event ) {
        switch ( event.keyCode ) {
            case 38: /*up*/
            case 87: /*W*/ this.input.move.jump = false; break;

            case 37: /*left*/
            case 65: /*A*/ this.input.move.left = false; break;

            case 39: /*right*/
            case 68: /*D*/ this.input.move.right = false; break;

            case 16: /*shift*/ this.input.sprint = false; break;
            case 32: /*space*/ this.input.move.jump = false; break;
        }
    };
};